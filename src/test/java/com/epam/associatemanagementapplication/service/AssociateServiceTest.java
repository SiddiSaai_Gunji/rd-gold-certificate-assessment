package com.epam.associatemanagementapplication.service;

import com.epam.associatemanagementapplication.dto.AssociateDTO;
import com.epam.associatemanagementapplication.dto.BatchDTO;
import com.epam.associatemanagementapplication.exception.AssociateException;
import com.epam.associatemanagementapplication.model.Associate;
import com.epam.associatemanagementapplication.model.Batch;
import com.epam.associatemanagementapplication.repository.AssociateRepository;
import com.epam.associatemanagementapplication.repository.BatchRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class AssociateServiceTest {


    @Mock
    AssociateRepository associateRepository;

    @Mock
    BatchRepository batchRepository;

    @InjectMocks
    AssociateServiceImpl associateService;

    Batch batch;
    Associate associate;
    AssociateDTO associateDTO;
    BatchDTO batchDTO;

    @BeforeEach
    void initValues() {
        batch = new Batch();
        batch.setId(1);
        batch.setName("siddhu");
        batch.setPractise("Test");
        batch.setStartDate(new Date());
        batch.setEndDate(new Date());

        associate = new Associate();
        associate.setId(1);
        associate.setName("siddhu");
        associate.seteMail("test@testing.com");
        associate.setGender("Male");
        associate.setCollege("EPAM");
        associate.setStatus("AVAILABLE");
        associate.setBatch(batch);
        batch.setAssociateList(new ArrayList<>());

        batchDTO = new BatchDTO();
        batchDTO.setId(1);
        batchDTO.setName("siddhu");
        batchDTO.setPractise("Test");
        batchDTO.setStartDate(new Date());
        batchDTO.setEndDate(new Date());

        associateDTO = new AssociateDTO();
        associateDTO.setId(1);
        associateDTO.setName("siddhu");
        associateDTO.setEMail("test@testing.com");
        associateDTO.setGender("Male");
        associateDTO.setCollege("EPAM");
        associateDTO.setStatus("AVAILABLE");
        associateDTO.setBatchDTO(batchDTO);

    }


    @Test
    void testAddNewAssociateIfBatchExist(){
        Mockito.when(batchRepository.findById(associateDTO.getBatchDTO().getId())).thenReturn(Optional.of(batch));
        Mockito.when(associateRepository.save(any(Associate.class))).thenReturn(associate);
        Mockito.when(batchRepository.save(any(Batch.class))).thenReturn(batch);
        assertNotNull(associateService.add(associateDTO));
        Mockito.verify(batchRepository).findById(associateDTO.getBatchDTO().getId());
        Mockito.verify(associateRepository).save(any(Associate.class));
        Mockito.verify(batchRepository).save(any(Batch.class));
    }


    @Test
    void testAddMewAssociateIfBatchNotExist(){
        Mockito.when(batchRepository.findById(associateDTO.getBatchDTO().getId())).thenReturn(Optional.empty());
        Mockito.when(associateRepository.save(any(Associate.class))).thenReturn(associate);
        Mockito.when(batchRepository.save(any(Batch.class))).thenReturn(batch);
        assertNotNull(associateService.add(associateDTO));
        Mockito.verify(batchRepository).findById(associateDTO.getBatchDTO().getId());
        Mockito.verify(associateRepository).save(any(Associate.class));
        Mockito.verify(batchRepository).save(any(Batch.class));
    }

    @Test
    void testUpdateAssociateWhenExists() throws AssociateException {
        Mockito.when(associateRepository.findById(associateDTO.getId())).thenReturn(Optional.of(associate));
        Mockito.when(associateRepository.save(any(Associate.class))).thenReturn(associate);
        assertNotNull(associateService.update(associateDTO));
        Mockito.verify(associateRepository).save(any(Associate.class));
        Mockito.verify(associateRepository).findById(associateDTO.getId());

    }

    @Test
    void testUpdateAssociateWhenNotExists() {
        Mockito.when(associateRepository.findById(associateDTO.getId())).thenReturn(Optional.empty());
        assertThrows(AssociateException.class,()->associateService.update(associateDTO));
        Mockito.verify(associateRepository).findById(associateDTO.getId());
    }

    @Test
    void testFetchAssociateListByGender(){
        Mockito.when(associateRepository.findAllByGender("Male")).thenReturn(Collections.singletonList(associate));
        assertNotNull(associateService.findByGender("Male"));
        Mockito.verify(associateRepository).findAllByGender("Male");
    }

    @Test
    void testDeleteAssociateById(){
        Mockito.doNothing().when(associateRepository).deleteById(1);
        assertDoesNotThrow(() -> associateService.remove(1));
        Mockito.verify(associateRepository).deleteById(1);
    }

}
