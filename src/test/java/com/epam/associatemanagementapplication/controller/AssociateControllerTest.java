package com.epam.associatemanagementapplication.controller;

import com.epam.associatemanagementapplication.dto.AssociateDTO;
import com.epam.associatemanagementapplication.dto.BatchDTO;
import com.epam.associatemanagementapplication.exception.AssociateException;
import com.epam.associatemanagementapplication.model.Associate;
import com.epam.associatemanagementapplication.model.Batch;
import com.epam.associatemanagementapplication.service.AssociateServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class AssociateControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AssociateServiceImpl associateService;

    Batch batch;
    Associate associate;
    AssociateDTO associateDTO;
    BatchDTO batchDTO;

    @BeforeEach
    void initValues() {
        batch = new Batch();
        batch.setId(1);
        batch.setName("siddhu");
        batch.setPractise("Test");
        batch.setStartDate(new Date());
        batch.setEndDate(new Date());

        associate = new Associate();
        associate.setId(1);
        associate.setName("siddhu");
        associate.seteMail("test@testing.com");
        associate.setGender("Male");
        associate.setCollege("EPAM");
        associate.setStatus("AVAILABLE");
        associate.setBatch(batch);
        batch.setAssociateList(Collections.singletonList(associate));

        batchDTO = new BatchDTO();
        batchDTO.setId(1);
        batchDTO.setName("siddhu");
        batchDTO.setPractise("Test");
        batchDTO.setStartDate(new Date());
        batchDTO.setEndDate(new Date());

        associateDTO = new AssociateDTO();
        associateDTO.setId(1);
        associateDTO.setName("siddhu");
        associateDTO.setEMail("test@testing.com");
        associateDTO.setGender("Male");
        associateDTO.setCollege("EPAM");
        associateDTO.setStatus("AVAILABLE");
        associateDTO.setBatchDTO(batchDTO);

    }

    @Test
    void testAddNewAssociate() throws Exception{
        Mockito.when(associateService.add(any(AssociateDTO.class))).thenReturn(associateDTO);
        mockMvc.perform(post("/rd/associates")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(associateDTO)))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("siddhu"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("test@testing.com"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("Male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.college").value("EPAM"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("AVAILABLE"));
        Mockito.verify(associateService).add(any(AssociateDTO.class));
    }

    @Test
    void testFetchingAssociatesThroughGender() throws  Exception{
        Mockito.when(associateService.findByGender("Male")).thenReturn(Collections.singletonList(associateDTO));
        mockMvc.perform(get("/rd/associates/Male"))
                .andExpect(status().isOk());
        Mockito.verify(associateService).findByGender("Male");
    }


    @Test
    void testUpdateAssociate() throws Exception{
        Mockito.when(associateService.update(any(AssociateDTO.class))).thenReturn(associateDTO);
        mockMvc.perform(put("/rd/associates")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(associateDTO)))
                .andExpect(status().isCreated());
        Mockito.verify(associateService).update(any(AssociateDTO.class));
    }

    @Test
    void testRemoveAssociate() throws Exception{
        Mockito.doNothing().when(associateService).remove(1);
        mockMvc.perform(delete("/rd/associates/1"))
                .andExpect(status().isNoContent());
        Mockito.verify(associateService).remove(1);
    }

    @Test
    void testWhenIllegibleDataInput() throws Exception{
        associateDTO.setName(null);
        mockMvc.perform(post("/rd/associates")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(associateDTO)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testWhenAssociateExceptionRaises() throws Exception{
        Mockito.when(associateService.findByGender("Male")).thenThrow(new AssociateException("not found"));
        mockMvc.perform(get("/rd/associates/Male"))
                .andExpect(status().isOk());
        Mockito.verify(associateService).findByGender("Male");

    }

    @Test
    void testWhenRuntimeExceptionRaises() throws Exception{
        Mockito.when(associateService.findByGender("Male")).thenThrow(new RuntimeException("not found"));
        mockMvc.perform(get("/rd/associates/Male"))
                .andExpect(status().isInternalServerError());
        Mockito.verify(associateService).findByGender("Male");
    }

}
