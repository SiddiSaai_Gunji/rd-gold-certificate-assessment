package com.epam.associatemanagementapplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition
public class AssociateManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssociateManagementApplication.class, args);
    }

}
