package com.epam.associatemanagementapplication.controller;

import com.epam.associatemanagementapplication.dto.AssociateDTO;
import com.epam.associatemanagementapplication.exception.AssociateException;
import com.epam.associatemanagementapplication.service.AssociateService;
import com.epam.associatemanagementapplication.util.StringConstants;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/rd")
@Slf4j
@RequiredArgsConstructor
public class AssociateController {
    private static final String ADD_NEW_ASSOCIATE_METHOD_NAME ="addNewAssociate";
    private static final String FETCH_ASSOCIATE_METHOD_NAME ="fetchAssociateByGender";
    private static final String UPDATE_ASSOCIATE_METHOD_NAME ="updateAssociate";
    private static final String REMOVE_ASSOCIATE_METHOD_NAME ="removeAssociate";

    private final AssociateService associateService;

    @PostMapping("/associates")
    ResponseEntity<AssociateDTO> addNewAssociate(@Valid @RequestBody AssociateDTO associateDTO){
        log.info(StringConstants.ENTERED_CONTROLLER_MESSAGE.getValue(), ADD_NEW_ASSOCIATE_METHOD_NAME, this.getClass().getName(), associateDTO.toString());
        associateDTO = associateService.add(associateDTO);
        ResponseEntity<AssociateDTO> response = new ResponseEntity<>(associateDTO, HttpStatus.CREATED);
        log.info(StringConstants.EXITING_CONTROLLER_MESSAGE.getValue(),ADD_NEW_ASSOCIATE_METHOD_NAME, this.getClass().getName());
        return response;
    }

    @GetMapping("/associates/{gender}")
    ResponseEntity<List<AssociateDTO>> fetchAssociateByGender(@Valid @NotBlank @PathVariable String gender){
        log.info(StringConstants.ENTERED_CONTROLLER_MESSAGE.getValue(), FETCH_ASSOCIATE_METHOD_NAME, this.getClass().getName(), gender);
        List<AssociateDTO> associateDTOList = associateService.findByGender(gender);
        ResponseEntity<List<AssociateDTO>> response = new ResponseEntity<>(associateDTOList, HttpStatus.OK);
        log.info(StringConstants.EXITING_CONTROLLER_MESSAGE.getValue(),FETCH_ASSOCIATE_METHOD_NAME, this.getClass().getName());
        return response;
    }

    @PutMapping("/associates")
    ResponseEntity<AssociateDTO> updateAssociate(@Valid @RequestBody AssociateDTO associateDTO) throws AssociateException {
        log.info(StringConstants.ENTERED_CONTROLLER_MESSAGE.getValue(), UPDATE_ASSOCIATE_METHOD_NAME, this.getClass().getName(), associateDTO.toString());
        associateDTO = associateService.update(associateDTO);
        ResponseEntity<AssociateDTO> response = new ResponseEntity<>(associateDTO, HttpStatus.CREATED);
        log.info(StringConstants.EXITING_CONTROLLER_MESSAGE.getValue(),UPDATE_ASSOCIATE_METHOD_NAME, this.getClass().getName());
        return response;
    }

    @DeleteMapping("/associates/{id}")
    ResponseEntity<AssociateDTO> removeAssociate(@Valid @NotNull @PathVariable int id){
        log.info(StringConstants.ENTERED_CONTROLLER_MESSAGE.getValue(), REMOVE_ASSOCIATE_METHOD_NAME, this.getClass().getName(), id);
        associateService.remove(id);
        log.info(StringConstants.EXITING_CONTROLLER_MESSAGE.getValue(),REMOVE_ASSOCIATE_METHOD_NAME, this.getClass().getName());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
