package com.epam.associatemanagementapplication.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
@Getter
@Setter
@ToString
public class BatchDTO {
    private int id;
    @NotBlank(message = "Please Give Name")
    private String name;
    @NotBlank(message = "Please Give Name")
    private String practise;
    @NotNull(message = "Please Give Name")
    private Date startDate;
    @NotNull(message = "")
    private  Date endDate;

}
