package com.epam.associatemanagementapplication.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class AssociateDTO {
    private int id;
    @NotBlank(message = "Give Name")
    private String name;
    @NotBlank(message = "Give E-Mail")
    private String eMail;
    @NotBlank(message = "Give Gender")
    private String gender;
    @NotBlank(message = "Give College")
    private String college;
    @NotBlank(message = "Give Status")
    private String status;
    @NotNull(message = "Give Batch Info")
    private BatchDTO batchDTO;
}
