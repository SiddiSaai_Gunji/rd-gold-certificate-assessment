package com.epam.associatemanagementapplication.repository;

import com.epam.associatemanagementapplication.model.Associate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssociateRepository extends JpaRepository<Associate, Integer> {
    List<Associate> findAllByGender(String gender);
}
