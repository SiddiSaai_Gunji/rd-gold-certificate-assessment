package com.epam.associatemanagementapplication.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Getter
public class Batch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String practise;
    @Column(nullable = false)
    private Date startDate;
    @Column(nullable = false)
    private  Date endDate;

    @OneToMany(mappedBy = "batch")
    @JsonIgnore
    private List<Associate> associateList = new ArrayList<>();


    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPractise(String practise) {
        this.practise = practise;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setAssociateList(List<Associate> associateList) {
        associateList.forEach(associate -> associate.setBatch(this));
        this.associateList = associateList;
    }

    @Override
    public String toString() {
        return "Batch{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", practise='" + practise + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
