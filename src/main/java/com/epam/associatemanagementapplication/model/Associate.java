package com.epam.associatemanagementapplication.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.ToString;

@Entity
@Getter
@ToString
public class Associate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String eMail;
    @Column(nullable = false)
    private String gender;
    @Column(nullable = false)
    private String college;
    @Column(nullable = false)
    private String status;

    @ManyToOne
    @JoinColumn(name = "batch_id")
    private Batch batch;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setBatch(Batch batch) {
        batch.getAssociateList().add(this);
        this.batch = batch;
    }
}
