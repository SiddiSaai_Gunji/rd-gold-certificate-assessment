package com.epam.associatemanagementapplication.util;

import com.epam.associatemanagementapplication.dto.AssociateDTO;
import com.epam.associatemanagementapplication.dto.BatchDTO;
import com.epam.associatemanagementapplication.model.Associate;
import com.epam.associatemanagementapplication.model.Batch;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ValueMapper {

    private static final String CONVERT_ASSOCIATE_DTO_TO_MODEL_METHOD_NAME = "convertAssociateDTOToModel";
    private static final String CONVERT_ASSOCIATE_MODEL_TO_DTO_METHOD_NAME = "convertAssociateModelTODTO";
    private static final String CONVERT_BATCH_DTO_TO_MODEL_METHOD_NAME = "convertBatchDTOToModel";
    public static final String CLASS_NAME = "ValueMapper";
    private ValueMapper() {
    }

    public static Associate convertAssociateDTOToModel(AssociateDTO associateDTO){
        log.info(StringConstants.ENTERED_SERVICE_MESSAGE.getValue(),CONVERT_ASSOCIATE_DTO_TO_MODEL_METHOD_NAME, CLASS_NAME, associateDTO.toString());
        Associate associate = new Associate();
        associate.setName(associateDTO.getName());
        associate.seteMail(associateDTO.getEMail());
        associate.setGender(associateDTO.getGender());
        associate.setCollege(associateDTO.getCollege());
        associate.setStatus(associateDTO.getStatus());
        log.info(StringConstants.EXITING_SERVICE_MESSAGE.getValue(), CONVERT_ASSOCIATE_DTO_TO_MODEL_METHOD_NAME,CLASS_NAME);
        return associate;
    }

    public static AssociateDTO convertAssociateModelTODTO(Associate associate){
        log.info(StringConstants.ENTERED_SERVICE_MESSAGE.getValue(),CONVERT_ASSOCIATE_MODEL_TO_DTO_METHOD_NAME, CLASS_NAME, associate.toString());
        AssociateDTO associateDTO = new AssociateDTO();
        BatchDTO batchDTO = new BatchDTO();
        Batch batch = associate.getBatch();
        batchDTO.setId(batch.getId());
        batchDTO.setName(batch.getName());
        batchDTO.setPractise(batch.getPractise());
        batchDTO.setStartDate(batch.getStartDate());
        batchDTO.setEndDate(batch.getEndDate());
        associateDTO.setId(associate.getId());
        associateDTO.setName(associate.getName());
        associateDTO.setBatchDTO(batchDTO);
        associateDTO.setEMail(associate.getEMail());
        associateDTO.setGender(associate.getGender());
        associateDTO.setCollege(associate.getCollege());
        associateDTO.setStatus(associate.getStatus());
        log.info(StringConstants.EXITING_SERVICE_MESSAGE.getValue(), CONVERT_ASSOCIATE_MODEL_TO_DTO_METHOD_NAME,CLASS_NAME);
        return associateDTO;
    }



    public static Batch convertBatchDTOToModel(BatchDTO batchDTO) {
        log.info(StringConstants.ENTERED_SERVICE_MESSAGE.getValue(),CONVERT_BATCH_DTO_TO_MODEL_METHOD_NAME, CLASS_NAME, batchDTO.toString());
        Batch batch = new Batch();
        batch.setName(batchDTO.getName());
        List<Associate> associateList = new ArrayList<>();
        batch.setAssociateList(associateList);
        batch.setStartDate(batchDTO.getStartDate());
        batch.setEndDate(batchDTO.getEndDate());
        batch.setPractise(batchDTO.getPractise());
        log.info(StringConstants.EXITING_SERVICE_MESSAGE.getValue(), CONVERT_BATCH_DTO_TO_MODEL_METHOD_NAME,CLASS_NAME);
        return batch;
    }

}
