package com.epam.associatemanagementapplication.service;

import com.epam.associatemanagementapplication.dto.AssociateDTO;
import com.epam.associatemanagementapplication.exception.AssociateException;

import java.util.List;

public interface AssociateService {

    String ADD_METHOD_NAME = "add";
    String UPDATE_METHOD_NAME = "update";
    String FIND_BY_GENDER_METHOD_NAME = "findByGender";
    String REMOVE_METHOD_NAME = "remove";
    AssociateDTO add(AssociateDTO associateDTO);

    AssociateDTO update(AssociateDTO associateDTO) throws AssociateException;

    List<AssociateDTO> findByGender(String gender);

    void remove(int id);
}
