package com.epam.associatemanagementapplication.service;

import com.epam.associatemanagementapplication.dto.AssociateDTO;
import com.epam.associatemanagementapplication.exception.AssociateException;
import com.epam.associatemanagementapplication.model.Associate;
import com.epam.associatemanagementapplication.model.Batch;
import com.epam.associatemanagementapplication.repository.AssociateRepository;
import com.epam.associatemanagementapplication.repository.BatchRepository;
import com.epam.associatemanagementapplication.util.StringConstants;
import com.epam.associatemanagementapplication.util.ValueMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AssociateServiceImpl implements AssociateService {



    private final AssociateRepository associateRepository;
    private final BatchRepository batchRepository;


    public AssociateDTO add(AssociateDTO associateDTO){
        log.info(StringConstants.ENTERED_SERVICE_MESSAGE.getValue(), ADD_METHOD_NAME, this.getClass().getName(), associateDTO.toString());
        Associate associate = ValueMapper.convertAssociateDTOToModel(associateDTO);
        AssociateDTO finalAssociateDTO = associateDTO;


        Batch batch;
        Optional<Batch> batchOptional = batchRepository.findById(associateDTO.getBatchDTO().getId());
        batch = batchOptional.orElseGet(() -> ValueMapper.convertBatchDTOToModel(finalAssociateDTO.getBatchDTO()));
        batch.getAssociateList().add(associate);
        batchRepository.save(batch);

        associate.setBatch(batch);
        associateRepository.save(associate);
        associateDTO = ValueMapper.convertAssociateModelTODTO(associate);
        log.info(StringConstants.EXITING_SERVICE_MESSAGE.getValue(), ADD_METHOD_NAME, this.getClass().getName());
        return associateDTO;
    }

    public AssociateDTO update(AssociateDTO associateDTO) throws AssociateException {
        log.info(StringConstants.ENTERED_SERVICE_MESSAGE.getValue(), UPDATE_METHOD_NAME, this.getClass().getName(), associateDTO.toString());
        AssociateDTO finalAssociateDTO = associateDTO;
        associateDTO = associateRepository.findById(associateDTO.getId()).map(dbAssociate -> {
            Associate associate = ValueMapper.convertAssociateDTOToModel(finalAssociateDTO);
            associate.setId(dbAssociate.getId());
            associate.setBatch(dbAssociate.getBatch());
            associateRepository.save(associate);
            return ValueMapper.convertAssociateModelTODTO(associate);
        }).orElseThrow(() -> {
            log.info(StringConstants.ERROR_MESSAGE.getValue(),UPDATE_METHOD_NAME);
            return new AssociateException("No Associate Found to Update");
        });
        log.info(StringConstants.EXITING_SERVICE_MESSAGE.getValue(), UPDATE_METHOD_NAME, this.getClass().getName());
        return associateDTO;
    }

    public List<AssociateDTO> findByGender(String gender){
        log.info(StringConstants.ENTERED_SERVICE_MESSAGE.getValue(), FIND_BY_GENDER_METHOD_NAME, this.getClass().getName(), gender);
        List<Associate> associateList = associateRepository.findAllByGender(gender);
        List<AssociateDTO> associateDTOList = new ArrayList<>();
        associateList.forEach( associate -> associateDTOList.add(ValueMapper.convertAssociateModelTODTO(associate)));
        log.info(StringConstants.EXITING_SERVICE_MESSAGE.getValue(), FIND_BY_GENDER_METHOD_NAME, this.getClass().getName());
        return associateDTOList;
    }

    public void remove(int id){
        log.info(StringConstants.ENTERED_SERVICE_MESSAGE.getValue(), REMOVE_METHOD_NAME, this.getClass().getName(), id);
        associateRepository.deleteById(id);
        log.info(StringConstants.EXITING_SERVICE_MESSAGE.getValue(), REMOVE_METHOD_NAME, this.getClass().getName());
    }

}
