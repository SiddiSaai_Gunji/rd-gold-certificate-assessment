package com.epam.associatemanagementapplication.exception;

public class AssociateException extends RuntimeException{
    public AssociateException(String message) {
        super(message);
    }
}
